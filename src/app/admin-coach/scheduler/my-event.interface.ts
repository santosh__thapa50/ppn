
// export interface MyEvent {
//     TaskID?: number;
//     OwnerID?: number;
//     Title?: string;
//     Description?: string;
//     Start?: Date;
//     End?: Date;
//     StartTimezone?: string;
//     EndTimezone?: string;
//     IsAllDay?: boolean;
//     RecurrenceException?: any;
//     RecurrenceID?: number;
//     RecurrenceRule?: string;
// }


export interface MyEvent{
  
		EventMetaId?: number,
		OwnerOrganizationId?: number,
		EventTitle?: string,
		EventStatusId?: number,
		EventStatusName?: string,
		EventTypeId?: number,
		EventType?:string,
		EventTypeLookUpName?: string,
		Start?:Date,
		End?: Date,
		Location?: string,
		Color?: string,
		AllDayFlag?: boolean,
		HasInvitees?: boolean,
		OwnerUserID?: number,
		RecurrenceID?: number,
		RecurrenceRule?: string,
		RecurrenceException?: any,
		AllowAddEdit?: boolean,
		EventMetaGuid?: string,
		SeasonTeamId?: number,
		JoinedCount?: number,
		IsPrivate?: boolean,
		ImportanceLevel?:  boolean,
		TeamRosterStatus?:  boolean,
		SeasonTeamName?: string,
		FeeAmount?: number,
		IsSignUpRequired?: boolean,
		ProgramTypeName?: string,
		Opponent?: string,
		GameScore?:  string,
		EventImage?:string,
		IsSeriesIndividual?: boolean,
		SeatsAvailable?: number,
		IsPublicEvent?: boolean,
		MaxMemberAllowed?: number,
		SignupedCount?: number,
		IsInvited?: boolean,
		OrganizationName?: string,
		SeasonName?: string,
		ProgramTemplateId?: number,
		GameRosterStatus?: boolean,
		SeasonTeamLogo?: string,
		// StartDateWithTimeZone?: Date,
		// EndDateWithTimeZone?: Date,
		EventTimeZone?: string,
		TimeZoneDisplayName?: string,
		TimeZoneShortName?: string,
		LocationForMap?: string,
		RecurrenceDates?:Date,
		StartDateWithEventTimezone?: Date,
		EndDateWithEventTimezone?: Date,
		EventTimeZoneShortName?: string
}