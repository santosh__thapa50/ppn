import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonuserRoutingModule } from './commonuser-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CommonuserRoutingModule
  ]
})
export class CommonuserModule { }
