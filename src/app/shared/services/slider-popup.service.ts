import { Injectable } from '@angular/core';
import * as $ from 'jquery';
@Injectable({
  providedIn: 'root'
})
export class SliderPopupService {

  constructor() { }

  public getLargestIndexOfOpenedSchedulerPopup () {
    var openedPopup = $('.k-overlay + .k-widget.k-window').css('z-index');
    return openedPopup;
};
  public firstTriggeredSlideId: any='';
  public openedSliderQue: any=[];
 public slideModalShow(popupId, offsetId, size, configs) {

      var configs = configs || { bgScroll: false };

      //console.log(configs);

      /* For preventing the body scroll while slider is on.*/
      if (this.firstTriggeredSlideId == '') {
          this.firstTriggeredSlideId = popupId;
          if (!configs.bgScroll) {
              $('body').toggleClass('affix');
          }
      } else if (this.firstTriggeredSlideId != '' && this.firstTriggeredSlideId == popupId) {
          this.firstTriggeredSlideId = '';
          if (!configs.bgScroll) {
              $('body').toggleClass('affix');
          }
      }


      /* Lets configure the sliders attributes.*/
      var popupSlide = popupId,
          popupSlideBody = popupId + 'Body',
          popupExpandId = popupId + 'CompExp  i',
          winHeight = $(window).height(),
          winWidth = $(window).width(),
          tabsOffset = $(offsetId).height(),
          height = winHeight - tabsOffset - 40;



      /* Lets check slider size.*/
      var slideWidth;

      //giving the slider full view while screen is less than 768px
      if ($(window).width() < 768) {
          size = 'F';
      }
      else {
          if (!size) {
              size = 'L';
          }
      }

      if (size == 'F') {
          slideWidth = '100%';
          tabsOffset = 0;
          height = winHeight;
      } else if (size == 'MF') { // for Medium width and Full height
          slideWidth = '75%';
          tabsOffset = 0;
          height = winHeight;
      } else if (size == 'L') {
          slideWidth = '95%';
      } else if (size == 'M') {
          slideWidth = '75%';
      } else if (size == 'S') {
          slideWidth = '50%';
      } else if (size == 'XS') {
          slideWidth = '30%';
      } else {
          slideWidth = '40%';
      }





      //if (size == 'F') {
      //    tabsOffset = 0;
      //    height = winHeight;
      //}

      /* Lets register the slider in Que.*/
      if ($.inArray(popupId, this.openedSliderQue) == -1) {
          this.openedSliderQue.push(popupId);
      } else {
          this.openedSliderQue = $.grep(this.openedSliderQue, function (value) {
              return value != popupId;
          });
      }
      var queIndex = $.inArray(popupId, this.openedSliderQue);



      /* Lets check if shceduler is present*/
      var scheduler_Z_Index;
      if (this.getLargestIndexOfOpenedSchedulerPopup() !== undefined) {
          scheduler_Z_Index = parseInt(this.getLargestIndexOfOpenedSchedulerPopup());
          //alert(scheduler_Z_Index);
      } else {
          scheduler_Z_Index = 10000;
          //alert('undefined');
      }



      /* Setting the slider's Z-Index. */
      if (queIndex < 1) {
          $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth, 'z-index': scheduler_Z_Index + 5 });
      }
      else {
          var prevOpenedSliderId = this.openedSliderQue[queIndex - 1];
          let zIndexVal = parseInt( $(prevOpenedSliderId).css('z-index'));
          zIndexVal = zIndexVal + 5;
          if (zIndexVal > scheduler_Z_Index) {
              $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth, 'z-index': zIndexVal });
          } else {
              $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth, 'z-index': scheduler_Z_Index + 5 });
          }
      }


      /* for slider overlay control */
      if (this.openedSliderQue.length == 0) {
          $('.slider-overlay').css({ 'display': 'none' });
      } else {
        let zIndexVal =  parseInt( $(prevOpenedSliderId).css('z-index'));
        $('.slider-overlay').css({ 'display': 'block' });
          if ($.inArray(popupId, this.openedSliderQue) == -1 || queIndex == 0) {
              var currZIndex = parseInt($(this.openedSliderQue[this.openedSliderQue.length - 1]).css('z-index'));
              $('.slider-overlay').css({ 'z-index': currZIndex - 1 });
          } else {
              $('.slider-overlay').css({ 'z-index':  zIndexVal - 1 });
          }
      }


      $(popupSlideBody).css({ 'height': height, 'width': 100 + '%' });
      $(popupExpandId).addClass('fa-expand').removeClass(' fa-compress');
      $(popupSlide).animate({ width: "toggle" }, 250, function () { });
      $(popupSlide).attr('size', size);
    //   setTimeout(function () {
    //       this.changeCoachPlayerIntoTeacherStudent();
    //   }, 1000);


  };

  public slideModalMinMax (popupId, offsetId, size, configs) {
      //console.log('ocuh');
      var popupSlide = popupId, popupSlideBody = popupId + 'Body',
          popupExpandId = popupId + 'CompExp  i',
          popupTitleBar = popupId + 'TitleBar',
          winHeight = $(window).height(),
          winWidth = $(window).width(),
          expandState = $(popupExpandId).hasClass('fa-expand');

      var slideWidth,
          queIndex = $.inArray(popupId, this.openedSliderQue);

      //giving the slider full view while screen is less than 768px
      if ($(window).width() < 768) {
          size = 'F';
      } else {
          if (!size) {
              size = $(popupSlide).attr('size');
          }
      }


      if (size == 'F') {
          slideWidth = '100%';
      } else if (size == 'MF') {
          slideWidth = '75%';
      } else if (size == 'L') {
          slideWidth = '95%';
      } else if (size == 'M') {
          slideWidth = '75%';
      } else if (size == 'S') {
          slideWidth = '50%';
      } else {
          slideWidth = '40%';
      }
      $(popupSlide).attr('size', size);
      if (expandState) {
          $(popupSlide).css({ 'top': 0, 'width': winWidth });
          $(popupSlideBody).css({ 'height': winHeight - 30, 'width': winWidth });
      } else {
          var tabsOffset = $(offsetId).height();
          var height = winHeight - $(offsetId).height() - 30;

          if (size == 'F' || size == 'MF') {
              tabsOffset = 0;
              height = winHeight;
          }
          $(popupSlide).css({ 'top': tabsOffset, 'margin-left': 0, 'width': slideWidth });
          $(popupSlideBody).css({ 'height': height, 'width': '100%' });
      }
      $(popupExpandId).toggleClass('fa-expand fa-compress');
  };

  public  changeCoachPlayerIntoTeacherStudent () {

    try {
        var isCoach = localStorage._UserType === "Coach";
        var isPE = isCoach ? (localStorage._OrgSportsName === "physicaleducation" || localStorage._OrgSportsName === "arts" || localStorage._OrgSportsName === "education" || localStorage._OrgSportsName === "Education") : (localStorage['_SelectedSportsName'] === "Physical Education" || localStorage['_SelectedSportsName'] === "Arts" || localStorage['_SelectedSportsName'] === "Education");
        if (isPE) {
            $('.k-view-coach >a.k-link:contains("coach"),.k-view-coach >a.k-link:contains("Coach"),option.yes-teacher-student:contains("coach"),option.yes-teacher-student:contains("Coach"),span.k-input:contains("coach"),span.k-input:contains("Coach"),ul.k-list >li:contains("coach"),ul.k-list >li:contains("Coach"),.teamManagement > div > span:contains("coach"),.teamManagement > div > span:contains("Coach"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("coach"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("Coach"),.yes-teacher-student:contains("coach"),.yes-teacher-student:contains("Coach"),.alert p:not(.not-teacher-student):contains("Coach"),.alert p:not(.not-teacher-student):contains("coach"),.alert span:not(.not-teacher-student):contains("Coach"),.alert span:not(.not-teacher-student):contains("coach"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("Coach"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("coach"),label:not(.not-teacher-student):contains("Coach"),label:not(.not-teacher-student):contains("coach"),h1:not(.not-teacher-student):contains("Coach"),h1:not(.not-teacher-student):contains("coach"),.has-sub a span:not(.not-teacher-student):contains("coach"),.has-sub a span:not(.not-teacher-student):contains("Coach"),th:not(.not-teacher-student):contains("Coach"),th:not(.not-teacher-student):contains("coach")').each(function () {
  
                var str = $(this).text();
  
                str = str.replace(/\Coaches/g, 'Teachers');
                str = str.replace(/\coaches/g, 'teachers');
                str = str.replace(/\COACHES/g, 'TEACHERS');
                str = str.replace(/\Coach/g, 'Teacher');
                str = str.replace(/\coach/g, 'teacher');
                str = str.replace(/\COACH/g, 'TEACHER');
  
                str = str.replace('Coaches', 'Teachers');
                str = str.replace('coaches', 'teachers');
                str = str.replace('COACHES', 'TEACHERS');
                str = str.replace('Coach', 'Teacher');
                str = str.replace('coach', 'teacher');
                str = str.replace('COACH', 'TEACHER');
  
                $(this).text(str);
            });
  
            $('.k-view-coach >a.k-link:contains("player"),.k-view-coach >a.k-link:contains("Player"),option.yes-teacher-student:contains("player"),option.yes-teacher-student:contains("Player"),span.k-input:contains("player"),span.k-input:contains("Player"),ul.k-list >li:contains("player"),ul.k-list >li:contains("Player"),.teamManagement > div > span:contains("player"),.teamManagement > div > span:contains("Player"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("player"),.popUpTitleBar > div > div:not(.not-teacher-student):contains("Player"),.yes-teacher-student:contains("player"),.yes-teacher-student:contains("Player"),.alert p:not(.not-teacher-student):contains("Player"),.alert p:not(.not-teacher-student):contains("player"),.alert p:not(.not-teacher-student):contains("players"),.alert span:not(.not-teacher-student):contains("Player"),.alert span:not(.not-teacher-student):contains("player"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("Player"),a:not(.dropdown-toggle):not(.not-teacher-student):contains("player"),label:not(.not-teacher-student):contains("Player"),label:not(.not-teacher-student):contains("player"),h1:not(.not-teacher-student):contains("Player"),h1:not(.not-teacher-student):contains("player"),.has-sub a span:not(.not-teacher-student):contains("player"),.has-sub a span:not(.not-teacher-student):contains("Player"),th:not(.not-teacher-student):contains("Player"),th:not(.not-teacher-student):contains("player")').each(function () {
                var str = $(this).text();
                if (str.length > 0) {
  
                    str = str.replace(/Player/g, 'Student');
                    str = str.replace(/player/g, 'student');
                    str = str.replace(/PLAYER/g, 'STUDENT');
  
                    str = str.replace('Player', 'Student');
                    str = str.replace('player', 'student');
                    str = str.replace('PLAYER', 'STUDENT');
  
                    $(this).text(str);
                }
            });
  
            $('input[type="text"]').each(function () {
                try {
                    var str = $(this).attr('placeholder');
  
                    if (str.length > 0) {
                        str = str.replace(/\Coaches/g, 'Teachers');
                        str = str.replace(/\coaches/g, 'teachers');
                        str = str.replace(/\COACHES/g, 'TEACHERS');
                        str = str.replace(/\Coach/g, 'Teacher');
                        str = str.replace(/\coach/g, 'teacher');
                        str = str.replace(/\COACH/g, 'TEACHER');
  
                        str = str.replace('Coaches', 'Teachers');
                        str = str.replace('coaches', 'teachers');
                        str = str.replace('COACHES', 'TEACHERS');
                        str = str.replace('Coach', 'Teacher');
                        str = str.replace('coach', 'teacher');
                        str = str.replace('COACH', 'TEACHER');
  
                        str = str.replace(/Player/g, 'Student');
                        str = str.replace(/player/g, 'student');
                        str = str.replace(/PLAYER/g, 'STUDENT');
  
                        str = str.replace('Player', 'Student');
                        str = str.replace('player', 'student');
                        str = str.replace('PLAYER', 'STUDENT');
  
                        $(this).attr('placeholder', str);
                    }
                } catch (e) {
  
                }
            });
        }
    } catch (e) {
  
    }
  };

 
}
