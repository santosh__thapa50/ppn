import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminCoachRoutingModule } from './admin-coach-routing.module';
import { SchedulerComponent } from './scheduler/scheduler.component';

import { InputsModule } from '@progress/kendo-angular-inputs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditorModule } from '@progress/kendo-angular-editor';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { SchedulerEditFormComponent, SchedulerService } from './scheduler';
import * as moment from 'moment';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

const providers=[];
@NgModule({
  declarations: [SchedulerComponent,
    SchedulerEditFormComponent],
  imports: [
    CommonModule,
    AdminCoachRoutingModule, 
    InputsModule,
    DropDownsModule,
    FormsModule,
    ReactiveFormsModule,
    EditorModule,
    ButtonsModule,
    DateInputsModule,
    SchedulerModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    InputsModule,
    HttpClientJsonpModule
  ],
  
})
export class AdminCoachModule { }


@NgModule({})
export class AdminCoachModuleExport{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AdminCoachModule,
      providers: providers
    }
  }
}