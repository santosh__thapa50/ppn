import { NgModule ,ModuleWithProviders} from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AccountRoutingModule} from './account-routing.module'
import { FormsModule ,ReactiveFormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';
import 'core-js/es7/reflect'
import { HttpClientModule, HTTP_INTERCEPTORS ,HttpClientJsonpModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
const providers = [];

@NgModule({
  declarations: [
  LoginComponent,
  RegisterComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientJsonpModule,
  ],
  providers:[]
})
export class AccountModule { }


@NgModule({})
export class AccountModuleExport{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AccountModule,
      providers: providers
    }
  }
}
