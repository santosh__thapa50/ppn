import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService, AuthenticationService, GlobalFunctionService ,LicenseAndRoleService} from '../../shared/services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    returnUrl: string;
    username: string;
    password: string;
    constructor(private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private global: GlobalFunctionService,
        private licenseAndRoleService: LicenseAndRoleService) { }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        this.authenticationService.login(this.username, this.password)
            .pipe(first())
            .subscribe(
                data => {
                    localStorage.setItem('token', data['access_token']);
                    localStorage._Username = data['userName'];
                    localStorage._RefreshToken = data['refresh_token'];
                    localStorage._UserType = data['UserType'] ? data['UserType'] : '';

                    localStorage._TokenExpiresIn = data['expires_in'];
                    localStorage._TokenIssued = data['.issued'];
                    localStorage._TokenExpires = data['.expires'];
                    localStorage._IsProfileFilled = data['IsProfileFilled'];
                    //localStorage._DaysLeft = data['DaysLeft'];
                    localStorage._UserId = data['UserId'];
                    localStorage._FirstName = data['FirstName'];
                    localStorage._LastName = data['LastName'];
                    localStorage._OrgLogo = "";
                    localStorage._OrgLogoPublic = "";
                    localStorage._IsSuperUser = data["IsSuperUser"] ? data["IsSuperUser"] : "false";
                    localStorage._IsEmailVerified = data["IsEmailVerified"];
                    localStorage._AccountCreatedDate = data["AccountCreatedDate"] ? data["AccountCreatedDate"] : "";
                    localStorage._UserEmail = data['UserEmail'] ? data['UserEmail'] : "";
                    localStorage._ExternalProviders = data['ExternalProviders'];
                    //  this.router.navigate([this.returnUrl]);

                    if (localStorage['_UserType'] === 'Coach') {

                        localStorage['_Roles'] = data['Roles'];
                        var rolelist = JSON.parse(data['Roles'])['ROLELIST'];
                        var currentRole = '';
                        if (rolelist.length > 0) {
                            var orgList, selectedOrg;
                            // var selectDefaultOrg = function () {
                                var defaultOrganizationId = JSON.parse(localStorage._Roles)["DefaultOrganizationId"];
                                console.log('dsada' + defaultOrganizationId );
                                var defaultOrganization = this.global.getMatchedListByProperty(rolelist, 'OrganizationId', 2);
                                console.log('org List' + JSON.stringify(defaultOrganization));

                                if (defaultOrganization) {
                                    selectedOrg = defaultOrganization[0];
                                }
                                else {
                                    orgList = this.global.getMatchedListByProperty(rolelist, 'RoleName', 'Org_Admin');
                                    if (orgList) {
                                        var org = this.global.getMatchedPropertyByProperty(rolelist, 'IsPrimary', true, null);
                                        if (org !== null) {
                                            //Load primary organization
                                            selectedOrg = org;
                                        }
                                        else if (orgList.length > 0) {
                                            //Load 1st organizagtion with Org_Admin role
                                            selectedOrg = orgList[0];
                                        }
                                    }
                                    else {
                                        //Load 1st associate organization
                                        selectedOrg = rolelist[0];
                                    }
                                }
                            // }

                    }


                    var setCoachSession = function (selectedOrg) {
                        currentRole = currentRole || 'NotSet';

                        if (selectedOrg && selectedOrg["OrganizationId"]) {
                            this.licenseAndRoleService.getOrgActiveLicense(selectedOrg["OrganizationId"]).then(function (res) {
                                res = res.Data;

                                var input = {};
                                if (res && res["LicenseTypeLookUpName"]) {

                                    var packageString = this.global.convertPackagesToString(res["LicensePackage"]);
                                    localStorage._LicenseType = res["LicenseTypeLookUpName"];
                                    localStorage._Packages = packageString;

                                    input = {
                                        LicenseType: res["LicenseTypeLookUpName"],
                                        RoleName: currentRole,
                                        Packages: packageString
                                    };
                                }
                                else {
                                    localStorage._LicenseType = "";
                                    localStorage._Packages = "";
                                    input = {
                                        LicenseType: "",
                                        RoleName: currentRole,
                                        Packages: ""
                                    };
                                }
                                var eventMetaId= null,psId= null ,baseURL='', psOrgId=null, psLocId=null;

                                this.licenseAndRoleService.setUserSession(input).then(function (r) {
                                    //$.get(baseURL + '/User/SetOrgRole/' + currentRole).success(function (dataRole) {
                                    if (!data['IsFirstLogin']) {

                                        this.http.get("/Register/GetDefaultEventId").then(function (res) {
                                            if (eventMetaId && eventMetaId !== "0") {
                                                //window.location = baseURL + "/admin-c#/event?eid=" + eventMetaId;
                                            }
                                            else if (res > 0) {
                                                localStorage._JoinToEventMetaId = res;
                                                //window.location = baseURL + "/admin-c#/event?eid=" + localStorage._JoinToEventMetaId;
                                            }
                                            else if (psId && psId !== '0') {
                                                //redirect to reservation calendar
                                                var redirectUrl = baseURL + '/admin-c#/reservation?psOrgId=' + psOrgId + '&psLocId=' + psLocId + '&psId=' + psId;
                                                this.licenseAndRoleService.hasRentalReservationPackage(psOrgId, redirectUrl);
                                                //window.open(baseURL + '/admin-c#/reservation?psOrgId=' + psOrgId + '&psLocId=' + psLocId + '&psId=' + psId, '_self');
                                            }
                                            //else if (localStorage['_OrgTypeLookupName'] === 'trainingfacility' && selectedOrg["EnableStationReservation"] && selectedOrg["EnableStationReservation"] !== '0') {
                                            //    var redirectUrl = baseURL + '/admin-c#/reservation';
                                            //    LicenseAndRoleService.hasRentalReservationPackage(psOrgId, redirectUrl);
                                            //    //window.location = baseURL + '/admin-c#/reservation';
                                            //}
                                            else {
                                                //window.location = baseURL + "/admin-c#/home";
                                            }
                                        });
                                    }
                                    else {
                                        if (eventMetaId && eventMetaId !== "0") {
                                            //window.location = baseURL + "/admin-c#/event?eid=" + eventMetaId;
                                        }
                                        else if (psId && psId !== '0') {
                                            //redirect to reservation calendar
                                            var redirectUrl = baseURL + '/admin-c#/reservation?psOrgId=' + psOrgId + '&psLocId=' + psLocId + '&psId=' + psId;
                                            this.licenseAndRoleService.hasRentalReservationPackage(psOrgId, redirectUrl);
                                            //window.open(baseURL + '/admin-c#/reservation?psOrgId=' + psOrgId + '&psLocId=' + psLocId + '&psId=' + psId, '_self');
                                        }
                                        //else if (localStorage['_OrgTypeLookupName'] === 'trainingfacility' && selectedOrg["EnableStationReservation"] && selectedOrg["EnableStationReservation"] !== '0') {
                                        //    var redirectUrl = baseURL + '/admin-c#/reservation';
                                        //    LicenseAndRoleService.hasRentalReservationPackage(psOrgId, redirectUrl);
                                        //    //window.location = baseURL + '/admin-c#/reservation';
                                        //}
                                        else {
                                            //window.location = baseURL + '/admin-c#/home';
                                        }
                                    }
                                });
                            }); //
                        } else {
                            // for no org case
                            var input = {
                                LicenseType: "",
                                RoleName: currentRole,
                                Packages: ""
                            };
                            try {
                                this.licenseAndRoleService.setUserSession(input).then(function (res) {
                                    // if (eventMetaId && eventMetaId !== "0") {
                                    //     window.location = baseURL + "/admin-c#/event?eid=" + eventMetaId;
                                    // } else {
                                    //     window.location = baseURL + "/admin-c#/home";
                                    // }
                                });
                            } catch (ex) {
                               // window.location = baseURL + "/admin-c#/home";
                            }
                        }
                    }

                }
                    this.router.navigate(['account/scheduler']);
                },
                error => {
                    this.alertService.error(error);
                });
            }
}


