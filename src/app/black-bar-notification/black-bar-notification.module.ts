import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlackBarNotificationRoutingModule } from './black-bar-notification-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BlackBarNotificationRoutingModule
  ]
})
export class BlackBarNotificationModule { }
