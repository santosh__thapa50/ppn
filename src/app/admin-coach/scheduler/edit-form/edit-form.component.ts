import { Component, Output, EventEmitter, Input, ViewEncapsulation } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { EditMode } from '@progress/kendo-angular-scheduler';
import '@progress/kendo-date-math/tz/regions/Europe';
import '@progress/kendo-date-math/tz/regions/NorthAmerica';
import { SchedulerService } from '../scheduler.service'
import { SchedulerComponent } from '../scheduler.component'
import { Scheduler } from 'rxjs';
@Component({
    selector: 'scheduler-edit-form',
    encapsulation: ViewEncapsulation.None,
    styles: [`
        #eventForm {
            max-width: 600px;
        }

        #eventForm .k-datetime-picker-wrapper .k-widget {
            display: inline-block;
            width: 150px;
            margin-right: 15px;
        }

        #eventForm .k-edit-label { width: 17%; }
        #eventForm .k-edit-field { width: 77%; }
    `],
    templateUrl: 'edit-form.component.html',

})
export class SchedulerEditFormComponent {
    public eventMeta = {
        EventTitle: "",
        EventType: "",
        Start: "",
        End: "",
        EventTypeId: 0,
        RecurrenceID: "",
        RecurrenceRule: "",
        IsAllDay: false
    }
    @Input()
    public isNew = false;

    @Input()
    public editMode: EditMode;
    public defaultEventType: { EventTypeName: string, EventTypeId: number };
    @Input()
    public set event(ev: any) {
        if (ev !== undefined) {
            this.editForm.reset(ev);
            this.active = true;
            this.eventMeta = ev;
        }
        this.defaultEventType={
            EventTypeName: this.eventMeta.EventType, EventTypeId: this.eventMeta.EventTypeId
        };
    }
    

    @Output()
    public cancel: EventEmitter<any> = new EventEmitter();

    @Output()
    public save: EventEmitter<any> = new EventEmitter();
    public uploadedfile = [];

    public file: File = null;

    public active = false;


    public editForm = new FormGroup({
        'EventTitle': new FormControl('', Validators.required),
        'EventType': new FormControl(),
        'Start': new FormControl('', Validators.required),
        'End': new FormControl('', Validators.required),
        'IsAllDay': new FormControl(false),
        'RecurrenceRule': new FormControl(),
        'RecurrenceID': new FormControl(),
        'Location': new FormControl()
    });


    public get isEditingSeries(): boolean {
        return this.editMode === EditMode.Series;
    }

    constructor(public formBuilder: FormBuilder,
        public schedulerService: SchedulerService,
        public schedulerComponent: SchedulerComponent,
    ) { }


    onSelectedFile(event) {
        if (event.target.files.length) {
            this.uploadedfile = event.target.files;
        }
    }

    handleEventChange(value) {
        this.eventMeta.EventTypeId=value.EventTypeId;
    }

    public multiarry: any = [];

    public onSave(eventMeta: any): void {
        //e.preventDefault();
        this.active = false;
        this.multiarry.eventMeta = eventMeta;
        this.multiarry.file = this.uploadedfile;
        this.save.emit(this.multiarry);
    }

    public onCancel(e: MouseEvent): void {
        e.preventDefault();
        this.active = false;
        this.cancel.emit();
    }
}
