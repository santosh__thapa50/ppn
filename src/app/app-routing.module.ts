import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './account/login/login.component';
import { AuthGuard } from './auth.guard'
const routes: Routes = [
  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule',
  },
  {
    path: 'admin-c',
    loadChildren: './admin-coach/admin-coach.module#AdminCoachModule'
  },
  { path: '**', redirectTo: './account/account.module#AccountModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
