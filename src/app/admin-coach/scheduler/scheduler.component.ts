import { Component, OnInit, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { SchedulerService } from './scheduler.service';
import { filter } from 'rxjs/operators';
import '@progress/kendo-date-math/tz/regions/Europe';
import '@progress/kendo-date-math/tz/regions/NorthAmerica';
import { resources } from './events-utc';
import { SliderPopupService } from '../../shared/services'
import {
    PopupService,
    PopupRef
} from '@progress/kendo-angular-popup';

import {
    CrudOperation,
    EditMode,
    EventClickEvent,
    RemoveEvent,
    SlotClickEvent,
    Resource
} from '@progress/kendo-angular-scheduler';
import { SchedulerEditFormComponent } from './edit-form/edit-form.component';
import { template } from '@angular/core/src/render3';

@Component({
    selector: 'app-scheduler',
    templateUrl: 'scheduler.component.html',
    styleUrls: ['scheduler.component.css']
})


export class SchedulerComponent {
    public selectedDate: Date = new Date();

    public editedEvent: any;
    public editMode: EditMode;
    public isNew: boolean;

    constructor(public schedulerService: SchedulerService,
        public openSlider: SliderPopupService
    ) { }

    public ngOnInit(): void {
        this.schedulerService.read();
    }

    public resources: Resource[] = resources;
    public slotDblClickHandler({ start, end, isAllDay }: SlotClickEvent): void {
        this.isNew = true;

        this.editMode = EditMode.Series;
        this.openSlider.slideModalShow('#coachReadOnlyTemplateSlider', '.page-header', 'F', '');
        this.editedEvent = {
            Start: start,
            End: end,
            IsAllDay: isAllDay
        };
    }

    public eventMeta: any = {};

    public eventDblClickHandler({ sender, event }: EventClickEvent): void {
        this.isNew = false;
        let dataItem = event.dataItem;
        this.eventMeta = dataItem;
        if (this.schedulerService.isRecurring(dataItem)) {
            sender.openRecurringConfirmationDialog(CrudOperation.Edit)

                // The dialog will emit `undefined` on cancel
                .pipe(filter(editMode => editMode !== undefined))
                .subscribe((editMode: EditMode) => {
                    if (editMode === EditMode.Series) {
                        dataItem = this.schedulerService.findRecurrenceMaster(dataItem);
                    }
                    this.openSlider.slideModalMinMax('#coachReadOnlyTemplateSlider', '.page-header', 'F', '');
                    this.editMode = editMode;
                    this.editedEvent = dataItem;
                });

        } else {

            this.openSlider.slideModalShow('#coachReadOnlyTemplateSlider', '.page-header', 'F', '');
            this.editMode = EditMode.Series;
            this.editedEvent = dataItem;
        }
    }

    public saveHandler(data: any): void {
        if (this.isNew) {
            this.schedulerService.create(data);
        } else {
            console.log('Data' + JSON.stringify(data));
            this.handleUpdate(data, data, this.editMode);
        }

    }


    public removeHandler({ sender, dataItem }: RemoveEvent): void {
        if (this.schedulerService.isRecurring(dataItem)) {
            sender.openRecurringConfirmationDialog(CrudOperation.Remove)
                // result will be undefined if the Dialog was closed
                .pipe(filter(editMode => editMode !== undefined))
                .subscribe((editMode) => {
                    this.handleRemove(dataItem, editMode);
                });
        } else {
            sender.openRemoveConfirmationDialog().subscribe((shouldRemove) => {
                if (shouldRemove) {
                    this.schedulerService.remove(dataItem);
                }
            });
        }
    }

    public cancelHandler(): void {
        this.editedEvent = undefined;
    }

    private handleUpdate(item: any, value: any, mode: EditMode): void {
        const service = this.schedulerService;
        if (mode === EditMode.Occurrence) {
            if (service.isException(item)) {
                service.update(item, value);
            } else {
                service.createException(item, value);
            }
        } else {
            // Item is not recurring or we're editing the entire series
            service.SaveEvent(item, value);
        }
    }

    private handleRemove(item: any, mode: EditMode): void {
        const service = this.schedulerService;
        if (mode === EditMode.Series) {
            service.removeSeries(item);
        } else if (mode === EditMode.Occurrence) {
            if (service.isException(item)) {
                service.remove(item);
            } else {
                service.removeOccurrence(item);
            }
        } else {
            service.remove(item);
        }
    }

    public coachReadOnlyTemplateSliderCompExp() {
        this.openSlider.slideModalMinMax('#coachReadOnlyTemplateSlider', '.page-header', 'F', '');

    }

    public coachReadOnlyTemplateSliderCloseBtn() {
        this.openSlider.slideModalShow('#coachReadOnlyTemplateSlider', '.page-header', 'F', '');

    }
}
