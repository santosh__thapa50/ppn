import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchedulerComponent } from './scheduler';
import { AuthGuard} from '../auth.guard';
const routes: Routes = [
  {
    path: 'admin-c/scheduler',
    component: SchedulerComponent,
    canActivate: [AuthGuard],
    // children: [
    //   {
    //     path: '',
    //     canActivateChild: [AuthGuard],
    //     children: [
    //     ]
    //   }
    // ]
  },
  {
    path:'account/scheduler',
    component: SchedulerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminCoachRoutingModule { }
