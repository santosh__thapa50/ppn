import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'core-js/es7/reflect'
import { HttpClientModule, HTTP_INTERCEPTORS ,HttpClientJsonpModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AccountModule, AccountModuleExport } from './account/account.module'
import { AccountRoutingModule } from './account/account-routing.module';
import { FormsModule ,ReactiveFormsModule } from '@angular/forms';
import { TokenInterceptorService, AuthenticationService,AlertService} from './shared/services';
import { AdminCoachModuleExport } from "./admin-coach/admin-coach.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SchedulerService } from './admin-coach/scheduler';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AccountModule,
    AccountRoutingModule,
    FormsModule,
    ReactiveFormsModule ,
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserAnimationsModule,
    AccountModuleExport.forRoot(),
    AdminCoachModuleExport.forRoot()
  ],
  providers: [
    AlertService,
    AuthenticationService,
    SchedulerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
