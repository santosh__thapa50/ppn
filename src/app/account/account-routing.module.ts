
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from '../auth.guard'
const routes: Routes = [
  {
    path: 'account',
    component: LoginComponent,
    canActivate: [AuthGuard],
  },
  {
      path: 'account/scheduler',
      redirectTo: 'admin-c/scheduler',
    }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }