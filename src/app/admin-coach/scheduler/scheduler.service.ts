import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { map ,  tap } from 'rxjs/operators';
import { BaseEditService, SchedulerModelFields } from '@progress/kendo-angular-scheduler';
import { parseDate } from '@progress/kendo-angular-intl';
import * as moment from 'moment';
import { MyEvent } from './my-event.interface';
import { Resource } from '@progress/kendo-angular-scheduler';
import { resources } from './events-utc';
import { Location } from '@angular/common';



const CREATE_ACTION = 'create';
const UPDATE_ACTION = 'update';
const REMOVE_ACTION = 'destroy';

const fields = {
  id: 'EventMetaId',
  title: 'EventTitle',
  description: 'Description',
  startTimezone: 'StartDateWithTimeZone',
  start: 'Start',
  end: 'End',
  endTimezone: 'EndDateWithTimeZone',
  isAllDay: 'AllDayFlag',
  recurrenceRule: 'RecurrenceRule',
  recurrenceId: 'RecurrenceID',
  recurrenceExceptions: 'RecurrenceException',
  eventType: 'EventType',
  color: 'Color'
};
@Injectable()
export class SchedulerService extends BaseEditService<MyEvent>{
  eventList: any = '';
  public formGroup: FormGroup;
 public resources: Resource[] = resources;

  public selectedDate: Date = new Date();

  public loading = false;
  public eventType: any =[];
  constructor(private http: HttpClient,
    private location: Location,
    private formBuilder: FormBuilder) {
    super(fields);
  }

  fetchEventList() {
    const date = moment().format('MM/DD/YYYY');
    const endDate = moment().subtract(30, 'days').format('MM/DD/YYYY');
    const params = new HttpParams()
      .set('filterByPersonalEvent', '')
      .set('filterBySignUpRequired', '')
      .set('filterByPaidOrFree', '')
      .set('filterByEventStatus', '')
      .set('filterByEventType', '')
      .set('selectedTeam', '')
      .set('SeasonTeamIds', '')
      .set('filterByStartDate', '02/24/2019')
      .set('filterByEndDate', '04/14/2019')
      .set('SignedupEventOnly', '')
      .set('OrgId', '2')
      .set('UserTimezone', '345')
      .set('IsMyAgenda', 'false')
      .set('ProgramType', '')
      .set('SeasonIds', '14190')
      .set('SportsId', '1')
      .set('UserId', '0');

    return this.http.get(`${environment.apiUrl}/api/Scheduler/GetEventList`, { params })
      .pipe(
        map(data => {
          this.eventList = data;
          this.serializeModels(this.eventList);
          return this.eventList;
        })
      );
  }

 public  getEventTypePreLoad() {
    const params = new HttpParams()
    .set('SportsId','1')
    .set('OrganizationId','2');
    return this.http.get(`${environment.apiUrl}/api/EventType/GetEventTypesV2`,{params : params} )
    .pipe(
      map(data => {
        this.eventType= data['Data'];
        return this.eventType;
      })
    );
}
 


  public read(): void {
    if (this.data.length) {
      this.source.next(this.data);
      return;
    }

    this.fetchEventList().subscribe(data => {
      this.data = data.map(item => this.readEvent(item));
      this.source.next(this.data);
    });
     this.getEventTypePreLoad().subscribe(data =>{
       this.eventType=data;
     });
  }

  protected save(created: MyEvent[], updated: MyEvent[], deleted: MyEvent[]): void {
    const completed = [];
    if (deleted.length) {
      // completed.push(this.fetch(REMOVE_ACTION, deleted));
    }

    if (updated.length) {
      // completed.push(this.fetch(UPDATE_ACTION, updated));
      completed.push(this.SaveEvent(UPDATE_ACTION, updated));
    }

    if (created.length) {
      // completed.push(this.fetch(CREATE_ACTION, created));
      completed.push(this.create(CREATE_ACTION, created));
    }
    //   zip(...completed).subscribe(() => this.read());
  }

  public create(action: any ,data?: any) {
    
    const formData: FormData = new FormData();
    if (data.file.length > 0) {
      formData.append('file', data.file[0]);
    }
    data.eventMeta.RecurrenceException = "";
    data.eventMeta.LocationId = "";
    data.eventMeta.UserTimezone = 345;
    data.eventMeta.DiscountIds = [];
    data.eventMeta.Remarks = "";
    data.eventMeta.OfferedSeasonTeamId = null;
    data.eventMeta.OfferedSeasonId = 0;
    data.eventMeta.SeasonId = 14190;

    formData.append('eventMeta', JSON.stringify(data.eventMeta));
    this.http.post(`${environment.apiUrl}api/Scheduler/SaveProgramEvent_v3`, formData).subscribe((res) => {
      console.log(res);
    });
  }


  public SaveEvent(action: any, data?: any) {
    const formData: FormData = new FormData();
    if (data.file.length > 0) {
      formData.append('file', data.file[0]);
    }
    data.eventMeta.RecurrenceException = "";
    data.eventMeta.LocationId = "";
    data.eventMeta.UserTimezone = 345;
    data.eventMeta.DiscountIds = [];
    data.eventMeta.Remarks = "";
    data.eventMeta.OfferedSeasonTeamId = null;
    data.eventMeta.OfferedSeasonId = 0;
    data.eventMeta.SeasonId = 14190;

    formData.append('eventMeta', JSON.stringify(data.eventMeta));
    this.http.post(`${environment.apiUrl}api/Scheduler/SaveProgramEvent_v3`, formData).subscribe((res) => {
      location.reload();
      console.log(res);
    });
  }

  private readEvent(item: any): MyEvent {
    return {
      ...item,
      Start: parseDate(item.StartDate),
      End: parseDate(item.EndDate),
      RecurrenceException: this.parseExceptions(item.RecurrenceException)
    };
  }

  private serializeModels(events: MyEvent[]): string {
    if (!events) {
      return '';
    }
    const data = events.map(event => ({
      ...event,
      RecurrenceException:
        this.serializeExceptions(event.RecurrenceException)
    }));
    
    return `&models=${JSON.stringify(data)}`;
  }

}

